package com.badwitsoftware.account;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class AccountResourceAssembler implements ResourceAssembler<Account, Resource<Account>> {

	@Override
	public Resource<Account> toResource(Account account) {
		return new Resource<Account>(account,
				linkTo(methodOn(AccountController.class).readAccount(account.getId())).withSelfRel(),
				linkTo(methodOn(AccountController.class).findAll()).withRel("accounts"));
	}

}
