package com.badwitsoftware.account;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

	@Autowired
	private AccountService service;

	@Autowired
	private AccountResourceAssembler assembler;

	@GetMapping(value = "/accounts", produces = MediaTypes.HAL_JSON_VALUE)
	Resources<Resource<Account>> findAll() {
		List<Resource<Account>> accounts = service.findAll().stream().map(assembler::toResource)
				.collect(Collectors.toList());

		return new Resources<>(accounts, linkTo(methodOn(AccountController.class).findAll()).withSelfRel());
	}

	@PostMapping("/accounts")
	ResponseEntity<Resource<?>> createAccount(@RequestBody Account account) throws URISyntaxException {
		Resource<?> resource = assembler.toResource(service.save(account));
		return ResponseEntity.created(new URI(resource.getId().expand().getHref())).body(resource);
	}

	@GetMapping(value = "/accounts/{id}", produces = MediaTypes.HAL_JSON_VALUE)
	Resource<Account> readAccount(@PathVariable Long id) {
		Account account = service.findById(id);
		return assembler.toResource(account);
	}

	@PutMapping("/accounts/{id}")
	ResponseEntity<?> updateAccount(@RequestBody Account newAccount, @PathVariable Long id) throws URISyntaxException {
		Account updatedAccount = service.findOptionalById(id).map(account -> {
			account.setEmail(newAccount.getEmail());
			account.setUsername(newAccount.getUsername());
			return service.save(account);
		}).orElseGet(() -> {
			newAccount.setId(id);
			newAccount.setRegistrationDate(new Date());
			return service.save(newAccount);
		});

		Resource<Account> resource = assembler.toResource(updatedAccount);
		return ResponseEntity.created(new URI(resource.getId().expand().getHref())).body(resource);
	}

	@DeleteMapping("/accounts/{id}")
	ResponseEntity<?> deleteAccount(@PathVariable Long id) {
		service.deleteById(id);
		return ResponseEntity.noContent().build();
	}

}
